The Puckett family has provided expert and trustworthy electrical services to the Garland, Texas area for 40 years. As a family owned and operated business, we've been open since 1976, passing the company down through three generations. For more information please contact (972) 864-0020!

Address: 2602 Industrial Ln, Garland, TX 75041, USA
Phone: 972-864-0020
